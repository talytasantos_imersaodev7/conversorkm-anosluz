var km = 10000000000000;
var constAnosLuzEmKm = 9.461e12;
var anosLuz = km / constAnosLuzEmKm;

anosLuz = anosLuz.toFixed(2);

alert("A distância em anos-luz é aproximadamente " + anosLuz);